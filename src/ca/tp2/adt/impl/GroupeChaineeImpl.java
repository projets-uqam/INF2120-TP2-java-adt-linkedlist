package ca.tp2.adt.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ca.tp2.adt.GroupeTda;
import ca.tp2.adt.PositionException;

/**
 * GroupeChaineeImpl : Classe pour implémenter l'interface GroupeTda<T> en
 * utilisant la classe Noeud<T> (liste chaine).
 */
public class GroupeChaineeImpl<T> implements GroupeTda<T> {

    private Noeud<T> premierNoeud; // La référence vers le premier noeud de la liste chaine

    private Noeud<T> Iterateur; // La référence vers la position courante

    private Noeud<T> IterateurPrecedent; // La référence vers la position precedente

    private int nbElements;  // Nombre d'élements dans la liste chaine

    /*
     * Constructeur par défaut.
     */
    public GroupeChaineeImpl() {

        premierNoeud = null;
        Iterateur = null;
        IterateurPrecedent = null;
        nbElements = 0;
    }

    /*
     * méthode qui retourne un iterator sur le groupe courant
     */
    @Override
    public Iterator<T> iterator() {

        if (Iterateur == null && IterateurPrecedent == null) {

            Iterateur = premierNoeud;
        }
        return this;

    }  // end iterator()

    /*
     * méthode qui vérifie s'il y a un élément à partir de la position courante
     * dans le groupe
     */
    @Override
    public boolean hasNext() {

        return Iterateur != null;

    }  // end hasNext()

    /*
     * méthode qui retourne l'élément à la position courante et repositionne le
     * curseur (l'itérateur) sur l'élément suivant.
     */
    @Override
    public T next() {

        T element = Iterateur.getElement();

        IterateurPrecedent = Iterateur;

        Iterateur = Iterateur.getSuivant();

        return element;

    }  // end next()

    public void remove() {
    }

    /*
     * méthode qui ajoute un élément au début du groupe courant, return true si
     * l'élément n'est pas null, il n'existe pas dans le groupe et qu'il a été
     * ajouté sinon false
     */
    @Override
    public boolean ajouterDebut(T element) {

        boolean ajouteDebut = false;

        if (element != null && !estElement(element)) {

            Noeud<T> unNoeud = new Noeud<T>(element);

            unNoeud.setSuivant(premierNoeud);

            premierNoeud = unNoeud;

            nbElements++;

            ajouteDebut = true;

        }

        return ajouteDebut;

    }  // end ajouterDebut(T element)

    /*
     * méthode qui ajoute un élément à la fin du groupe courant, return true si
     * l'élément n'est pas null, il n'existe pas dans le groupe et qu'il a été
     * ajouté sinon false
     */
    @Override
    public boolean ajouterFin(T element) {

        boolean ajouterFin = false;

        if (estVide() && element != null) {

            premierNoeud = new Noeud<T>(element);
            nbElements++;
            ajouterFin = true;

        } else {

            Noeud<T> unNoeudCourant = premierNoeud;

            while (unNoeudCourant.getSuivant() != null) {

                unNoeudCourant = unNoeudCourant.getSuivant();
            }

            if (element != null && !estElement(element)) {

                Noeud<T> unNoeud = new Noeud<T>(element);

                unNoeudCourant.setSuivant(unNoeud);

                nbElements++;

                ajouterFin = true;
            }
        }

        return ajouterFin;

    }  // end ajouterFin(T element)

    /*
     * méthode qui ajoute un élément à une position donnée dans le groupe
     * courant et décale les autres éléments vers la fin, throws PositionException
     * si l'indice n'est pas dans le bon intervalle et return true si l'indice
     * est bon, l'élément n'est pas null, il n'existe pas dans le groupe et
     * qu'il est ajouté sinon false
     */
    @Override
    public boolean ajouter(int indice, T element) throws PositionException {

        boolean ajoute = false;

        if (indice < 0 || indice > nbElements()) { // on depasse le bon
            // intervalle

            throw new PositionException("L'indice est invalide");

        } else if (indice == 0) {

            ajouterDebut(element);
            ajoute = true;

        } else if (indice == nbElements()) {

            ajouterFin(element);
            ajoute = true;

        } else // cas normal
        if (element != null && !estElement(element)) {

            Noeud<T> unNoeudCourant = premierNoeud;
            Noeud<T> unNoeudPrecedent = premierNoeud;

            for (int i = 0; i < indice; i++) {

                unNoeudPrecedent = unNoeudCourant;
                unNoeudCourant = unNoeudCourant.getSuivant();
            }

            Noeud<T> unNoeud = new Noeud<T>(element);
            unNoeudPrecedent.setSuivant(unNoeud);
            unNoeud.setSuivant(unNoeudCourant);
            ajoute = true;
            nbElements++;

        }

        return ajoute;

    }  // end ajouter(int indice, T element)

    /*
     * méthode qui ajoute les éléments du groupe passé en paramètre au début du
     * groupe courant, return le tableau liste des éléments qui n'ont pas été
     * ajoutés, null si tous les éléments ont été ajoutés ou si le groupe passé
     * en paramètre est nul ou vide
     */
    @Override
    public List<T> ajouterDebut(GroupeTda<T> groupe) {

        // creation du tableau avec les éléments qui n'ont pas été ajoutés
        List<T> groupeExisteDeja = new ArrayList<T>();

        T elementCourant;

        while (groupe.iterator().hasNext()) {

            elementCourant = groupe.iterator().next();

            if (!ajouterDebut(elementCourant)) {

                groupeExisteDeja.add(elementCourant);
            }
        }

        /*
	       * si tous les éléments ont été ajoutés ou si le groupe passé en
	       * paramètre est nul ou vide le groupeExisteDeja est null
         */
        if (groupeExisteDeja.isEmpty()) {

            groupeExisteDeja = null;
        }

        return groupeExisteDeja;

    }  // end ajouterDebut(GroupeTda<T> groupe)

    /*
     * méthode qui ajoute les éléments du groupe passé en paramètre à la fin du
     * groupe courant, return le tableau liste des éléments qui n'ont pas été
     * ajoutés, null si tous les éléments ont été ajoutés ou si le groupe passé
     * en paramètre est nul ou vide
     */
    @Override
    public List<T> ajouterFin(GroupeTda<T> groupe) {

        // creation du tableau avec les éléments qui n'ont pas été ajoutés
        List<T> groupeExisteDeja = new ArrayList<T>();

        T elementCourant;

        while (groupe.iterator().hasNext()) {

            elementCourant = groupe.iterator().next();

            if (!ajouterFin(elementCourant)) {

                groupeExisteDeja.add(elementCourant);
            }
        }

        /*
	       * si tous les éléments ont été ajoutés ou si le groupe passé en
	       * paramètre est nul ou vide le groupeExisteDeja est null
         */
        if (groupeExisteDeja.isEmpty()) {

            groupeExisteDeja = null;
        }

        return groupeExisteDeja;

    }  // end ajouterFin(GroupeTda<T> groupe)

    /*
     * méthode qui compare le groupe courant à cellui passé en paramètre, return
     * null si le groupe courant contient tous les éléments du groupe passé en
     * paramètre, sinon un tableau liste des éléments qui n'existent pas dans le
     * groupe courant est retourné
     */
    @Override
    public List<T> comparer(GroupeTda<T> groupe) {

        // creation du tableau avec les éléments qui n'existent pas dans le
        // groupe courant
        List<T> groupeInexistent = new ArrayList<T>();

        T elementCourant;

        while (groupe.iterator().hasNext()) {

            elementCourant = groupe.iterator().next();

            if (!estElement(elementCourant)) {

                groupeInexistent.add(elementCourant);
            }
        }

        /*
	       * si le groupe courant contient tous les éléments du groupe passé en
	       * paramètre, le groupeInexistent est null
        */
        if (groupeInexistent.isEmpty()) {

            groupeInexistent = null;
        }

        return groupeInexistent;

    }  // end comparer(GroupeTda<T> groupe)

    /*
     * méthode qui vérifie si le groupe courant contient l'élément passé en
     * paramètre, return true si l'élément existe, sinon faux
     */
    @Override
    public boolean estElement(T element) {

        boolean estElement = false;

        if (!estVide() || element != null) {

            Noeud<T> noeudCourant = premierNoeud;

            while (noeudCourant != null) {

                if (noeudCourant.getElement().equals(element)) {

                    estElement = true;

                }
                noeudCourant = noeudCourant.getSuivant();
            }
        }
        return estElement;

    }  // end estElement(T element)

    /*
     * méthode qui retourne le nombre d'éléments du groupe courant, return le
     * nombre total des éléments du groupe courant
     */
    @Override
    public int nbElements() {

        return nbElements;

    }  // end nbElements()

    /*
     * méthode qui supprime l'élément passé en paramètre du groupe courant,
     * return true si l'élément est supprimé, sinon faux
     */
    @Override
    public boolean supprimer(T element) {

        boolean supprime = false;

        if (!estVide() || element != null) {

            boolean elementTrouve = false;
            Noeud<T> unNoeudCourant = premierNoeud;
            Noeud<T> unNoeudPrecedent = null;
            T elementCourant = null;

            while (unNoeudCourant != null && !elementTrouve) {

                elementCourant = unNoeudCourant.getElement();

                if (element.equals(elementCourant)) {

                    elementTrouve = true;

                } else {

                    unNoeudPrecedent = unNoeudCourant;
                    unNoeudCourant = unNoeudCourant.getSuivant();
                }
            }

            if (elementTrouve) {

                /*
		             * liste avec un seul element ou l'element a supprimer est le
		             * premier dans la liste
                 */
                if (unNoeudPrecedent == null) {

                    premierNoeud = premierNoeud.getSuivant();
                    nbElements--;
                    supprime = true;

                } else {

                    unNoeudPrecedent.setSuivant(unNoeudCourant.getSuivant());
                    nbElements--;
                    supprime = true;
                }
            }
        }

        return supprime;

    }  // end supprimer(T element)

    /*
     * méthode qui supprime tous les éléments du groupe passé en paramètre du
     * groupe courant. return le tableau des éléments qui n'ont pas été
     * supprimés, null si tous les éléments ont été supprimés ou si le groupe
     * passé en paramètre est null ou vide
     */
    @Override
    public List<T> supprimer(GroupeTda<T> groupe) {

        // creation du tableau avec les éléments qui n'ont pas été supprimés
        List<T> groupeNonSuprimes = new ArrayList<T>();

        T elementCourant;

        while (groupe.iterator().hasNext()) {

            elementCourant = groupe.iterator().next();

            if (!supprimer(elementCourant)) {

                groupeNonSuprimes.add(elementCourant);
            }
        }

        /*
        * si tous les éléments ont été supprimés le groupeNonSuprimes est null
        */
        if (groupeNonSuprimes.isEmpty()) {

            groupeNonSuprimes = null;
        }

        return groupeNonSuprimes;

    }  // end supprimer(GroupeTda<T> groupe)

    /*
     * méthode qui remplace un élément par un autre dans le groupe courant
     * l'élément à remplacer doit exister et le nouveau élément ne doit pas être
     * null aucun remplacement si le groupe courant contient déjà le nouveau
     * élément return true si le remplacement a été fait, sinon faux
     */
    @Override
    public boolean remplacer(T elementARemplacer, T nouveauElement) {

        boolean remplace = false;

        if (nouveauElement != null && !estElement(nouveauElement)) {

            boolean elementTrouve = false;
            Noeud<T> unNoeudCourant = premierNoeud;
            Noeud<T> unNoeudPrecedent = null;
            T elementCourant = null;

            while (unNoeudCourant != null && !elementTrouve) {

                elementCourant = unNoeudCourant.getElement();

                if (elementARemplacer.equals(elementCourant)) {

                    elementTrouve = true;
                } else {

                    unNoeudPrecedent = unNoeudCourant;
                    unNoeudCourant = unNoeudCourant.getSuivant();
                }
            }

            /*
	           * liste avec un seul element ou l'element a remplacer est le
	           * premier dans la liste
             */
            if (elementTrouve) {

                if (unNoeudPrecedent == null) {

                    premierNoeud.setElement(nouveauElement);
                    remplace = true;

                } else {

                    unNoeudCourant.setElement(nouveauElement);
                    remplace = true;
                }
            }
        }
        return remplace;

    } // end remplacer(T elementARemplacer, T nouveauElement)

    /*
     * méthode qui remplace un élément à une position donnée dans le groupe
     * courant en écrasant l'ancien élément. throws PositionException si
     * l'indice n'est pas dans le bon intervalle return true si l'indice est
     * bon, l'élément n'est pas null, il n'existe pas dans le groupe et que le
     * remplacement a été fait, sinon false.
     */
    @Override
    public boolean remplacer(int indice, T element) throws PositionException {

        boolean remplace = false;

        if (indice < 0 || indice > nbElements()) { // on depasse le bon
            // intervalle

            throw new PositionException("L'indice est invalide");

        } else if (indice == 0) {

            remplacer(premierNoeud.getElement(), element);
            remplace = true;

        } else if (indice == nbElements()) {

            Noeud<T> unNoeudCourant = premierNoeud;

            while (unNoeudCourant.getSuivant() != null) {

                unNoeudCourant = unNoeudCourant.getSuivant();
            }

            remplacer(unNoeudCourant.getElement(), element);
            remplace = true;

        } else { // cas normal

            Noeud<T> unNoeudCourant = premierNoeud;

            for (int i = 0; i < indice; i++) {

                unNoeudCourant = unNoeudCourant.getSuivant();
            }

            remplacer(unNoeudCourant.getElement(), element);
            remplace = true;

        }

        return remplace;

    }  // end remplacer(int indice, T element)

    /*
     * méthode qui vérifie si le groupe courant est vide return true si le
     * groupe courant est vide, sinon false
     */
    @Override
    public boolean estVide() {

        boolean estVide = false;

        if (premierNoeud == null) {

            estVide = true;
        }
        return estVide;

    } // end estVide()

    /*
     * méthode qui vide le groupe courant.
     */
    @Override
    public void vider() {

        premierNoeud = null;

        nbElements = 0;

    } // end vider()

    /*
     * méthode qui return le HashMap de tous les éléments du groupe courant et
     * leurs positions, null si le groupe courant est vide.
     */
    @Override
    public Map<Integer, T> elements() {

        Map<Integer, T> map = null;

        if (!estVide()) {

            map = new HashMap<Integer, T>();

            Noeud<T> unNoeudCourant = premierNoeud;

            for (int i = 0; i < nbElements(); i++) {

                map.put(i, unNoeudCourant.getElement());

                unNoeudCourant = unNoeudCourant.getSuivant();
            }
        }

        return map;

    } // end elements()

} // end GroupeChaineeImpl<T>
