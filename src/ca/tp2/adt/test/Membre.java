package ca.tp2.adt.test;

/**
 * Membre : Cette classe correspond à l'élément T et
 * sera utilisé pour faire les tests unitaires
 */
public class Membre {

	// Déclaration des attributs
	private String identifiant;
	private String nom;
	private String prenom;


	/**
	 * Constructeur sans argument.
	 */
	public Membre() {
		super();
	}


	/**
	 * @param identifiant L'identifiant
	 * @param nom Le nom
	 * @param prenom Le prénom
	 */

	public Membre(String identifiant, String nom, String prenom) {
		this.identifiant = identifiant;
		this.nom = nom;
		this.prenom = prenom;
	}

	/**
	 * @return l'identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant l'identifiant à initialiser
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return le nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom le nom à initialiser
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return le prénom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom le prénom à initialiser
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object unAutreObjet) {

		boolean estEgal = false;
		// Si les 2 objets sont identiques, on doit retourner
		// vrai comme résultat.
	    if (this == unAutreObjet) {
			estEgal = true;
		} else if (unAutreObjet != null &&
				   this.getClass() == unAutreObjet.getClass()) {
			// Sachant que "unAutreObjet" n'est pas null et que "unAutreObjet"
			// et l'objet courant sont de même type, on peut se permettre
			// la conversion de "UnAutreObjet" en Membre
			Membre unMembre = (Membre)unAutreObjet;
			// Tester si les deux (2) objets ont le même identifiant.
			estEgal = (identifiant.equals(unMembre.identifiant));
		}
	    return estEgal;
	}
}
