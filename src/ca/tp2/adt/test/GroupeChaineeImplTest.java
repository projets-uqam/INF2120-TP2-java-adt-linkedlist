package ca.tp2.adt.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.tp2.adt.impl.GroupeChaineeImpl;
import ca.tp2.adt.test.Membre;
import ca.tp2.adt.GroupeTda;
import ca.tp2.adt.PositionException;

/**
 * GroupeChaineeImplTest : Classe pour tester toutes les méthodes de la classe GroupeChaineeImpl
 */

public class GroupeChaineeImplTest {

	GroupeTda<Membre> unGroupeChaine;

	Membre membre01;
	Membre membre02;
	Membre membre03;
	Membre membre04;

	@Before
	public void setUp() throws Exception {

		unGroupeChaine = new GroupeChaineeImpl<Membre>();


		membre01 = new Membre("AA01", "Abraham", "Alex");
		membre02 = new Membre("MM02", "Matte", "Martin");
		membre03 = new Membre("SM03", "Silve", "Maxim");
		membre04 = new Membre("PE04", "Pointe", "Eric");
	}

	@After
	public void tearDown() throws Exception {

		unGroupeChaine = null;

		membre01 = null;
		membre02 = null;
		membre03 = null;
		membre04 = null;
	}

	// methode pour parcourir unGroupeChaine de type: GroupeTda<Membre>

	private Membre [] parcourirGroupe() {

		Membre [] tab = new Membre[unGroupeChaine.nbElements()];
		int i = 0;

		while(unGroupeChaine.iterator().hasNext()){
			tab [i] = unGroupeChaine.iterator().next();
			i++;
		}

		return tab;
	}

	// Teste la methode iterator ()

	@Test
	public void testIterator() {

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);

		// retourne un iterator sur le groupe courant

		Iterator<Membre> iteratorGroupe = unGroupeChaine.iterator();

		Membre [] tab = new Membre[unGroupeChaine.nbElements()];
		int i = 0;

		while(iteratorGroupe.hasNext()){
			tab [i] = iteratorGroupe.next();
			i++;
		}

		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);

	}

	// Teste la methode hasNext ()

	@Test
	public void testHasNext (){

		// le groupe n'a pas de next AVANT l'ajoute

		assertFalse(unGroupeChaine.iterator().hasNext());

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

		// le groupe a de next APRES l'ajoute

		assertTrue(unGroupeChaine.iterator().hasNext());

	}

	// Teste la methode next ()

	@Test
	public void testNext (){

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);


		Membre [] tab = new Membre[unGroupeChaine.nbElements()];
		int i = 0;

	// Retourne l'élément à la position courante et repositionne le curseur sur l'élément suivant

		while(unGroupeChaine.iterator().hasNext()){
			tab [i] = unGroupeChaine.iterator().next();
			i++;
		}

		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);

	}

	// Teste la methode AjouterDebut () Scenario 01

	@Test
	public void testAjouterDebutScenario01 (){

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

	// Scenario 01 : l'élément n'est pas null et il n'existe pas dans le groupe courant

		unGroupeChaine.ajouterDebut(membre04);

		Membre [] tab = parcourirGroupe();

		assertEquals(4, unGroupeChaine.nbElements());

	//La position du premier élément du groupe est occupee pour le dernier element ajouté

		assertEquals(tab[0], membre04);
		assertEquals(tab[1], membre03);
		assertEquals(tab[2], membre02);
		assertEquals(tab[3], membre01);

	}

	// Teste la methode AjouterDebut () Scenario 02

		@Test
		public void testAjouterDebutScenario02 (){

			unGroupeChaine.ajouterDebut(membre01);
			unGroupeChaine.ajouterDebut(membre02);
			unGroupeChaine.ajouterDebut(membre03);

			assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements AVANT l'ajoute: 3

		// Scenario 02 : l'élément n'est pas null MAIS il existe deja dans le groupe courant

			unGroupeChaine.ajouterDebut(membre02);

			Membre [] tab = parcourirGroupe();

			assertEquals(3, unGroupeChaine.nbElements()); // nombre d'elements APRES l'ajoute: 3

		//La position du premier élément du groupe est occupee pour le dernier element ajouté

			assertEquals(tab[0], membre03);
			assertEquals(tab[1], membre02);
			assertEquals(tab[2], membre01);

		}

	// Teste la methode AjouterDebut () Scenario 03

	@Test
	public void testAjouterDebutScenario03() {

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);

		assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements AVANT
		// l'ajoute: 3

		// Scenario 03 : l'élément est null

		membre04 = null;

		unGroupeChaine.ajouterDebut(membre04);

		Membre [] tab = parcourirGroupe();

		assertEquals(3, unGroupeChaine.nbElements()); // nombre d'elements APRES
		// l'ajoute: 3

		// La position du premier élément du groupe est occupee pour le dernier
		// element ajouté

		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre01);

	}

	// Teste la methode AjouterFin () Scenario 01

		@Test
		public void testAjouterFinScenario01 (){

			unGroupeChaine.ajouterFin(membre01);
			unGroupeChaine.ajouterFin(membre03);
			unGroupeChaine.ajouterFin(membre02);

			assertEquals(3, unGroupeChaine.nbElements());

		// Scenario 01 : l'élément n'est pas null et il n'existe pas dans le groupe courant

			unGroupeChaine.ajouterFin(membre04);

			Membre [] tab = parcourirGroupe();

			assertEquals(4, unGroupeChaine.nbElements());

		//La position du dernier élément du groupe est occupee pour le dernier element ajouté

			assertEquals(tab[0], membre01);
			assertEquals(tab[1], membre03);
			assertEquals(tab[2], membre02);
			assertEquals(tab[3], membre04);

		}

	// Teste la methode AjouterFin () Scenario 02

	@Test
	public void testAjouterFinScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements AVANT l'ajoute: 3

		// Scenario 02 : l'élément n'est pas null MAIS il existe deja dans le groupe courant

		unGroupeChaine.ajouterFin(membre02);

		Membre [] tab = parcourirGroupe();

		assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements APRES l'ajoute: 3

		// La position du dernier élément du groupe est occupee pour le dernier
		// element ajouté

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);

	}

	// Teste la methode AjouterFin () Scenario 03

	@Test
	public void testAjouterFinScenario03() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements AVANT
														// l'ajoute: 3

		// Scenario 03 : l'élément est null

		membre04 = null;

		unGroupeChaine.ajouterFin(membre04);

		Membre [] tab = parcourirGroupe();

		assertEquals(3, unGroupeChaine.nbElements());// nombre d'elements APRES
														// l'ajoute: 3

		// La position du dernier élément du groupe est occupee pour le dernier
		// element ajouté

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);

	}

	// Teste la methode ajouter (indice, element) Scenario 01

	@Test
	public void testAjouterScenario01() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

		/*Scenario 01 : l'indice est dans le bon intervalle,
						l'élément n'est pas null et il n'existe pas dans le
						groupe courant */

		try {

			unGroupeChaine.ajouter(2, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre [] tab = parcourirGroupe();

		assertEquals(4, unGroupeChaine.nbElements());

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre04);// Ajoute l'élément à la position donnée (2)
		assertEquals(tab[3], membre03);

	}

	// Teste la methode ajouter (indice, element) Scenario 02

	@Test
	public void testAjouterScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

		/*Scenario 02 : l'indice N'EST PAS dans le bon intervalle,
						l'élément n'est pas null et il n'existe pas dans le
						groupe courant */

		try {

			unGroupeChaine.ajouter(7, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre [] tab = parcourirGroupe();

		assertEquals(3, unGroupeChaine.nbElements());

		// La position du premier élément du groupe est considéré comme 0.
		//l'élément n'a été pas ajouté et l'exception a été atrapper

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);

	}

	// Teste la methode ajouter (indice, element) Scenario 03

	@Test
	public void testAjouterScenario03() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

		/*
		 * Scenario 03 : l'indice est egal 0, l'élément n'est
		 * pas null et il n'existe pas dans le groupe courant
		 */

		try {

			unGroupeChaine.ajouter(0, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		assertEquals(4, unGroupeChaine.nbElements());

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre04);// Ajoute l'élément à la position donnée(0)
		assertEquals(tab[1], membre01);
		assertEquals(tab[2], membre02);
		assertEquals(tab[3], membre03);

	}

	// Teste la methode ajouter (indice, element) Scenario 04

	@Test
	public void testAjouterScenario04() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		assertEquals(3, unGroupeChaine.nbElements());

		/*
		 * Scenario 04 : l'indice est egal au nombre d'elements, l'élément n'est pas null et il
		 * n'existe pas dans le groupe courant
		 */

		try {

			unGroupeChaine.ajouter(3, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		assertEquals(4, unGroupeChaine.nbElements());

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);
		assertEquals(tab[3], membre04);// Ajoute l'élément à la position donnée(3)

	}

	// Teste la methode ajouter (indice, element) Scenario 05

	@Test
	public void testAjouterScenario05() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		/*
		 * Scenario 05 : l'indice est dans le bon intervalle, l'élément EST
		 * null et il EXISTE dans le groupe courant
		 */

		membre03 = null;

		try {

			unGroupeChaine.ajouter(2, membre02); // il EXISTE dans le groupe courant
			unGroupeChaine.ajouter(0, membre03); // l'élément EST null

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		assertEquals(2, unGroupeChaine.nbElements());// pas d'ajoute

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);

	}

	// Teste la methode ajouterDebut (groupe) Scenario 01

	@Test
	public void testAjouterDebutGroupeScenario01() {

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 01: les éléments du groupe passé en paramètre N'EXISTENT PAS
		// dans le groupe

		GroupeTda<Membre> groupeAAjouter = new GroupeChaineeImpl<Membre>();

		groupeAAjouter.ajouterFin(membre03);
		groupeAAjouter.ajouterFin(membre04);

		List<Membre> groupeExisteDeja = new ArrayList<Membre>();

		groupeExisteDeja = unGroupeChaine.ajouterDebut(groupeAAjouter);

		assertNull(groupeExisteDeja); // il est null si tous les éléments ont été ajoutés

		assertEquals(4, unGroupeChaine.nbElements());

		Membre[] tab = parcourirGroupe();

		assertEquals(tab [0], membre04);
		assertEquals(tab [1], membre03);
		assertEquals(tab [2], membre02);
		assertEquals(tab [3], membre01);

	}

	// Teste la methode ajouterDebut (groupe) Scenario 02

	@Test
	public void testAjouterDebutGroupeScenario02() {

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 02: les éléments du groupe passé en paramètre EXISTENT dans le groupe

		GroupeTda<Membre> groupeAAjouter = new GroupeChaineeImpl<Membre>();

		groupeAAjouter.ajouterDebut(membre01);
		groupeAAjouter.ajouterDebut(membre02);


		List<Membre> groupeExisteDeja = new ArrayList<Membre>();

		groupeExisteDeja = unGroupeChaine.ajouterDebut(groupeAAjouter);

		Iterator<Membre> itt = groupeExisteDeja.iterator();
		Membre [] tab = new Membre[groupeExisteDeja.size()];
		int i = 0;

		while(itt.hasNext()){
			tab [i] = itt.next();
			i++;
		}

		//tableau avec les éléments qui n'ont pas été ajoutés

		assertEquals(tab [0], membre02);
		assertEquals(tab [1], membre01);

		assertEquals(2, unGroupeChaine.nbElements());

	}

	// Teste la methode ajouterFin (groupe) Scenario 01

	@Test
	public void testAjouterFinGroupeScenario01() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 01: les éléments du groupe passé en paramètre N'EXISTENT PAS
		// dans le groupe

		GroupeTda<Membre> groupeAAjouter = new GroupeChaineeImpl<Membre>();

		groupeAAjouter.ajouterFin(membre03);
		groupeAAjouter.ajouterFin(membre04);

		List<Membre> groupeExisteDeja = new ArrayList<Membre>();

		groupeExisteDeja = unGroupeChaine.ajouterFin(groupeAAjouter);

		assertNull(groupeExisteDeja); // il est null si tous les éléments ont été ajoutés

		assertEquals(4, unGroupeChaine.nbElements());

		Membre[] tab = parcourirGroupe();

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);
		assertEquals(tab[3], membre04);

	}

	// Teste la methode ajouterFin (groupe) Scenario 02

	@Test
	public void testAjouterFinGroupeScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 02: les éléments du groupe passé en paramètre EXISTENT dans
		// le groupe

		GroupeTda<Membre> groupeAAjouter = new GroupeChaineeImpl<Membre>();

		groupeAAjouter.ajouterFin(membre01);
		groupeAAjouter.ajouterFin(membre02);

		List<Membre> groupeExisteDeja = new ArrayList<Membre>();

		groupeExisteDeja = unGroupeChaine.ajouterFin(groupeAAjouter);

		Iterator<Membre> itt = groupeExisteDeja.iterator();
		Membre[] tab = new Membre[groupeExisteDeja.size()];
		int i = 0;

		while (itt.hasNext()) {
			tab[i] = itt.next();
			i++;
		}

		//tableau avec les éléments qui n'ont pas été ajoutés

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);

		assertEquals(2, unGroupeChaine.nbElements());

	}

	// Teste la methode comparer (groupe) Scenario 01

	@Test
	public void testComparerGroupeScenario01() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 01: les éléments du groupe passé en paramétre N'EXISTENT PAS
		// dans le groupe

		GroupeTda<Membre> groupeAComparer = new GroupeChaineeImpl<Membre>();

		groupeAComparer.ajouterFin(membre03);
		groupeAComparer.ajouterFin(membre04);

		List<Membre> groupeInexistent = new ArrayList<Membre>();

		groupeInexistent = unGroupeChaine.comparer(groupeAComparer);

		Iterator<Membre> itt = groupeInexistent.iterator();
		Membre[] tab = new Membre[groupeInexistent.size()];
		int i = 0;

		while (itt.hasNext()) {
			tab[i] = itt.next();
			i++;
		}

		// tableau avec les éléments qui n'existent pas dans le groupe courant

		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre04);

		assertEquals(2, unGroupeChaine.nbElements());

	}

	// Teste la methode comparer (groupe) Scenario 02

	@Test
	public void testComparerGroupeScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);
		unGroupeChaine.ajouterFin(membre04);

		assertEquals(4, unGroupeChaine.nbElements());

		// Scenario 02: les éléments du groupe passé en paramètre EXISTENT dans
		// le groupe

		GroupeTda<Membre> groupeAComparer = new GroupeChaineeImpl<Membre>();

		groupeAComparer.ajouterFin(membre02);
		groupeAComparer.ajouterFin(membre03);

		List<Membre> groupeInexistent = new ArrayList<Membre>();

		groupeInexistent = unGroupeChaine.comparer(groupeAComparer);

		assertNull(groupeInexistent); // il est null si tous les éléments
									  //existent dans le groupe courant

		assertEquals(4, unGroupeChaine.nbElements());

	}

	// Teste la methode supprimer (), Scenario 01 - 02 - 03

	@Test

	public void testSupprimer(){

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

	// Scenario 01 : l'élément existe dans le groupe courant

		assertEquals(3, unGroupeChaine.nbElements()); // nombre d'éléments AVANT suprimer(): 3
		assertTrue(unGroupeChaine.supprimer(membre03));// l'élément est supprimé
		assertEquals(2, unGroupeChaine.nbElements()); // nombre d'éléments AVANT suprimer(): 2

	// Scenario 02 : l'élément n'est existe pas dans le groupe courant

		assertFalse(unGroupeChaine.supprimer(membre04));// l'élément n'a pas été supprimé

	/*Scenario 03 : l'élément est l'unique dans le groupe courant
					ou est le premier dans le groupe courant */

		unGroupeChaine.supprimer(membre02);
		assertTrue(unGroupeChaine.supprimer(membre01));// l'élément est supprimé
		assertEquals(0, unGroupeChaine.nbElements()); // nombre d'éléments AVANT suprimer(): 0

	}

	// Teste la methode supprimer (groupe) Scenario 01

	@Test
	public void testSupprimerGroupeScenario01() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);

		assertEquals(2, unGroupeChaine.nbElements());

		// Scenario 01: les éléments du groupe passé en paramètre N'EXISTENT PAS
		// dans le groupe

		GroupeTda<Membre> groupeASupprimer = new GroupeChaineeImpl<Membre>();

		groupeASupprimer.ajouterFin(membre03);
		groupeASupprimer.ajouterFin(membre04);

		List<Membre> groupeNonSuprimes = new ArrayList<Membre>();

		groupeNonSuprimes = unGroupeChaine.supprimer(groupeASupprimer);

		Iterator<Membre> itt = groupeNonSuprimes.iterator();
		Membre[] tab = new Membre[groupeNonSuprimes.size()];
		int i = 0;

		while (itt.hasNext()) {
			tab[i] = itt.next();
			i++;
		}

		//tableau liste (ArrayList<T>) des éléments qui n'ont pas été supprimés

		assertEquals(tab[0], membre03);
		assertEquals(tab[1], membre04);

		assertEquals(2, unGroupeChaine.nbElements());

	}

	// Teste la methode supprimer (groupe) Scenario 02

	@Test
	public void testSupprimerGroupeScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);
		unGroupeChaine.ajouterFin(membre04);

		assertEquals(4, unGroupeChaine.nbElements());

		// Scenario 02: les éléments du groupe passé en paramètre EXISTENT dans le groupe

		GroupeTda<Membre> groupeASupprimer = new GroupeChaineeImpl<Membre>();

		groupeASupprimer.ajouterFin(membre02);
		groupeASupprimer.ajouterFin(membre03);

		List<Membre> groupeNonSupprime = new ArrayList<Membre>();

		groupeNonSupprime = unGroupeChaine.supprimer(groupeASupprimer);

		assertNull(groupeNonSupprime); // il est null si tous les éléments ont été supprimés

		assertEquals(2, unGroupeChaine.nbElements());

		Membre[] tab = parcourirGroupe();

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre04);

	}

	// Teste la methode remplacer (T elementARemplacer, T nouveauElement)
	// Scenario 01 - 02 - 03

	@Test

	public void testRemplacer() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		/* Scenario 01 : l'élément a remplacer existe dans le groupe courant
						 et le nouveau element n'est existe pas dans le groupe courant*/

		assertEquals(3, unGroupeChaine.nbElements()); // nombre d'éléments AVANT
														// remplacer: 3

		assertTrue(unGroupeChaine.remplacer(membre02, membre04));// l'élément est remplacer

		assertEquals(3, unGroupeChaine.nbElements()); // nombre d'éléments AVANT
														// remplacer: 3

		Membre[] tab = parcourirGroupe();

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre04);// l'élément est remplacer
		assertEquals(tab[2], membre03);

		// Scenario 02 : le nouveau élément existe dans le groupe courant

		assertFalse(unGroupeChaine.remplacer(membre03, membre04));// l'élément n'a pas été remplacé

		// Scenario 03 : l'élément a remplacer n'est existe pas dans le groupe courant

		assertFalse(unGroupeChaine.remplacer(membre02, membre03));// l'élément n'a pas été remplacé

	}

	// Teste la methode remplacer (int indice, T element)
	// Scenario 01

	@Test

	public void testRemplacerAvecIndiceScenario01() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		// Scenario 01 : le nouveau element n'est existe pas dans le groupe courant

		try {

			unGroupeChaine.remplacer(1, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();


		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre04);// l'élément est remplace, position : 1
		assertEquals(tab[2], membre03);

	}

	// Teste la methode remplacer (int indice, T element)
	// Scenario 02

	@Test

	public void testRemplacerAvecIndiceScenario02() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		// Scenario 02 : le nouveau element existe dans le groupe courant

		try {

			unGroupeChaine.remplacer(1, membre03);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);// l'élément n'a pas été remplacé
		assertEquals(tab[2], membre03);

	}

	// Teste la methode remplacer (int indice, T element)
	// Scenario 03

	@Test

	public void testRemplacerAvecIndiceScenario03() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		// Scenario 03 : l'indice choisi est le dernier element dans le groupe

		try {

			unGroupeChaine.remplacer(2, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre04);// l'élément est remplace, position : 2

	}

	// Teste la methode remplacer (int indice, T element)
	// Scenario 04

	@Test

	public void testRemplacerAvecIndiceScenario04() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		// Scenario 04 : l'indice choisi est le premier element dans le groupe

		try {

			unGroupeChaine.remplacer(0, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		// La position du premier élément du groupe est considéré comme 0.

		assertEquals(tab[0], membre04);// l'élément est remplace, position : 0
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);
	}

	// Teste la methode remplacer (int indice, T element)
	// Scenario 05

	@Test

	public void testRemplacerAvecIndiceScenario05() {

		unGroupeChaine.ajouterFin(membre01);
		unGroupeChaine.ajouterFin(membre02);
		unGroupeChaine.ajouterFin(membre03);

		// Scenario 05: l'indice N'EST PAS dans le bon intervalle

		try {

			unGroupeChaine.remplacer(7, membre04);

		} catch (PositionException e) {

			assertEquals("PositionException", e.getClass().getSimpleName());
		}

		Membre[] tab = parcourirGroupe();

		// La position du premier élément du groupe est considéré comme 0.
		//l'élément n'a été pas ajouté et l'exception a été atrapper

		assertEquals(tab[0], membre01);
		assertEquals(tab[1], membre02);
		assertEquals(tab[2], membre03);
	}

	// Teste la methode estElement ()

	@Test
	public void testEstElement(){

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);

		// Scenario 01: l'élément existe déja dans le groupe courant
		assertTrue(unGroupeChaine.estElement(membre02));

		// Scenario 02 : l'élément n'existe pas dans le groupe courant
		assertFalse(unGroupeChaine.estElement(membre03));

		// Scenario 03 : l'élément est null
		membre04 = null;
		assertFalse(unGroupeChaine.estElement(membre04));

	}

	// Teste la methode nbElements ()

	@Test
	public void testNbElements() {

		assertEquals(0, unGroupeChaine.nbElements()); // nombre d'éléments : 0

		unGroupeChaine.ajouterDebut(membre01);
		unGroupeChaine.ajouterDebut(membre02);
		unGroupeChaine.ajouterDebut(membre03);

	    assertEquals(3, unGroupeChaine.nbElements()); // nombre d'éléments : 3

	}

	// Teste la methode estVide ()

		@Test
		public void testEstVide() {

			//return true si le groupe courant est vide

			assertTrue(unGroupeChaine.estVide());

			unGroupeChaine.ajouterDebut(membre01);
			unGroupeChaine.ajouterDebut(membre02);

			//return false si le groupe courant n'est pas vide

			assertFalse(unGroupeChaine.estVide());

		}

		// Teste la methode vider ()

		@Test
		public void testVider() {

			unGroupeChaine.ajouterDebut(membre01);
			unGroupeChaine.ajouterDebut(membre02);
			unGroupeChaine.ajouterDebut(membre03);

			// le groupe n'est pas vide

			assertEquals(3, unGroupeChaine.nbElements());

			assertFalse(unGroupeChaine.estVide());

			// le groupe est vide APRES vider()

			unGroupeChaine.vider();

			assertEquals(0, unGroupeChaine.nbElements());

			assertTrue(unGroupeChaine.estVide());
		}

		// Teste la methode elements ()

		@Test
		public void testElements() {

			unGroupeChaine.ajouterFin(membre01);
			unGroupeChaine.ajouterFin(membre02);
			unGroupeChaine.ajouterFin(membre03);

			// la taille du HashMap (clé - valeur) est: 3

			assertEquals(3, unGroupeChaine.elements().size());

			// La position du premier élément du groupe est considéré comme 0

			assertEquals(membre01,unGroupeChaine.elements().get(0));
			assertEquals(membre02,unGroupeChaine.elements().get(1));
			assertEquals(membre03,unGroupeChaine.elements().get(2));
		}

}// end GroupeChaineeImplTest
