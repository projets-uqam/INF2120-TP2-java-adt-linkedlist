package ca.tp2.adt;

/**
 * UQAM - Automne 2015
 * INF2120 - Groupe 20 - TP2
 *
 * Classe pour gérer les exceptions liées à une position dans un intervalle
 *
 * @author Ismael Doukoure
 * @version 2 novembre 2015
 */
@SuppressWarnings("serial")
public class PositionException extends Exception {

	/**
	 * Constructeur sans argument
	 */
	public PositionException() {
        super();
    }

	/**
	 * Permet d'initialiser le message
	 * @param message Message à afficher
	 */
	public PositionException(String message) {
        super(message);
    }

}
